// still need to import React to all components who have JSX kasi pag nagcompile may React.blabla
import React, { Component } from 'react';

// same as yung sa taas
//const Component = React.Component;

// Functional Component example:
//
// const SearchBar = () => {
// 	return <input />;
// };

//Class-based componnent = use when you need added functionality vs functional component
// SearchBar has functionality of React {Component}
// every class-based component must  have a render()
class SearchBar extends Component {
	// constructor gets called whenever may class based component. constructor is for initializing stuff
	constructor(props) {
		// super from Component
		super(props);

		// properties inside are the things we want to record for the state
		this.state = {
			term: ''
		};
	}

	// for every render function, return a jsx
	render() {
		// using arrow functions
		return (
			<div className="search-bar">
				<input
					value={this.state.term}
					onChange={event => this.onInputChange(event.target.value)}
				/>
			</div>
		);
	}

	// events
	onInputChange(term) {
		this.setState({ term });
		this.props.onSearchTermChange(term);
	}
}

export default SearchBar;
